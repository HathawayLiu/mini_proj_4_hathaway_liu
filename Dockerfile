# Use the official Rust image as a builder
FROM rust:1-slim-bookworm AS builder

# Create a new empty shell project
RUN USER=root cargo new --bin dice_roller
WORKDIR /dice_roller

# Copy the Cargo.toml and Cargo.lock files and build the dependencies
COPY Cargo.toml Cargo.lock ./
RUN cargo build --release
RUN rm src/*.rs

# Copy the source code
COPY ./src ./src
COPY ./static ./static

# Build the application for release
RUN rm -f ./target/release/deps/dice_roller*
RUN cargo build --release

# Final base image
FROM bitnami/minideb:bookworm

# Copy the build artifact from the builder stage and set permissions
COPY --from=builder /dice_roller/target/release/dice_roller /usr/local/bin

ENV ROCKET_ENV=production

# Set the binary as the entrypoint of the container
ENTRYPOINT ["/usr/local/bin/dice_roller"]

# Expose the port the server is running on
EXPOSE 8080