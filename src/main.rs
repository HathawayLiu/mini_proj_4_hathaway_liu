use actix_files::Files;
use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use rand::Rng;

// Dice rolling function using random number generator
async fn roll_dice() -> impl Responder {
    let result = rand::thread_rng().gen_range(1..=6).to_string();
    HttpResponse::Ok().body(format!("Dice Roll: {}", result))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/roll_dice", web::get().to(roll_dice))  // Add route for dice rolling
            .service(Files::new("/", "./static").index_file("index.html")) // Serve static files
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}